package main

import(
  "fmt"
  "net/http"
  "io/ioutil"
  "os"
)

func main() {
  // Read the first argument
  imgUrl := os.Args[1]
  // Get a response object
  resp, _ := http.Get(imgUrl)
  // Print the StatusCode
  fmt.Println(resp.StatusCode)
  // Read all from the Body (which is an Reader interface)
  bytes, _ := ioutil.ReadAll(resp.Body)
  // Get the content type
  contentType := resp.Header.Get("Content-Type")
  var fileExtension string
  switch contentType {
    case "image/jpeg": fileExtension = "jpg"
    case "image/gif": fileExtension = "gif"
    case "image/png": fileExtension = "png"
  }
  // Writes a file called image with the given bytes
  ioutil.WriteFile("image." + fileExtension, bytes, 0777)
}
