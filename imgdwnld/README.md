# imgdwnld

## Docker
The [`Dockerfile`](Dockerfile) can be used to execute either `go build`
or `go run`.
But you have to manually adjust it.

### Build the image
First you have to build the image:
```
docker build -t imgdwnld .
```

### Run it
To run the image you have to put your Golang source directory
into the `/go/src/app` directory in the container:
```
docker run -v ${PWD}:/go/src/app imgdwnld
```
The output is either an `app` executable
or some an `image.png`
