package main

import(
  "net/http"
  "io/ioutil"
  "bytes"
  "mime/multipart"
  "io"
  "encoding/json"
  "os"
  "log"
  "github.com/atotto/clipboard"
)

type fileIoResponse struct {
  Link string `json:"link"`
}

func main() {
  filePath, fileName := filePathAndNameFromArgumens()
  reader := readFile(filePath)

  writer, body := createFormData(fileName, reader)

  responseBody := doRequest(writer, body)
  defer responseBody.Close()

  link := linkFromResponse(responseBody)
  clipboard.WriteAll(link)
}

func filePathAndNameFromArgumens() (string, string) {
  if len(os.Args) <= 1 {
    log.Fatal("First argument should be the path to the file to upload")
  }

  filePath := os.Args[1]
  fileInfo, error := os.Stat(filePath)
  if error != nil {
    if os.IsNotExist(error) {
      log.Fatal(error)
      return "", ""
    }
  }
  return filePath, fileInfo.Name()
}

func readFile(filePath string) *bytes.Reader {
  contentAsByteArray, _ := ioutil.ReadFile(filePath)
  reader := bytes.NewReader(contentAsByteArray)
  return reader
}

func createFormData(fileName string, reader *bytes.Reader) (*multipart.Writer, bytes.Buffer) {
  body := bytes.Buffer{}
  writer := multipart.NewWriter(&body)
  part, _ := writer.CreateFormFile("file", fileName)
  io.Copy(part, reader)
  writer.Close()
  return writer, body
}

func doRequest(writer *multipart.Writer, body bytes.Buffer) io.ReadCloser {
  request, _ := http.NewRequest("POST", "https://file.io", &body)
  request.Header.Add("Content-Type", writer.FormDataContentType())
  client := http.Client{}
  response, _ := client.Do(request)
  return response.Body
}

func linkFromResponse(responseBody io.ReadCloser) string {
  bytes, _ := ioutil.ReadAll(responseBody)
  respStruct := &fileIoResponse{}
  json.Unmarshal(bytes, respStruct)
  return respStruct.Link
}
