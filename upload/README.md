# Upload

This will upload a file to https://file.io.

You only have to run
```
go run go.main /path/to/the/file
```
After the file was uploaded the URL
to the uploaded file will be added to your clipboard.
