package main

import(
  "fmt"
  "strings"
  "bufio"
  "os"
  "strconv"
  "math/rand"
  "time"
)

type player struct {
  name string
  infected bool
  x int
  y int
}

func main() {
  welcome()

  players := createPlayers()
  fmt.Println()
  infectPlayer(players)
  fmt.Println()
  buildMapAndPlacePlayers(players)
  fmt.Println()

  // TODO: Loop over the players
  // and ask what todo.
  // They can move left/right/top/bottom
  // if possible with map size.
  // All all players are infected -> game is over
  for {
    question("")
  }
}

// welcome prints the welcome message
// and some explanations
func welcome() {
  fmt.Println("Welcome to Virus!\nLeave the game by pressing 'Q' or 'q'")
  fmt.Println("")
}

// createPlayers generates the players
// who want to player and return a slice
// of players.
func createPlayers() []player {
  playersString := question("How many want to play")
  fmt.Println("Ok... there are \"" + playersString + "\" Players. Please give them a name.")
  playersNumber, _ := strconv.Atoi(playersString)

  players := []player{}
  for i := 1; i <= playersNumber; i++ {
    playerNumberString := strconv.Itoa(i)
    playerName := question("Player " + playerNumberString + " name")
    players = append(players, player { name : playerName })
  }
  return players
}

// infectPlayer infect a random player from the
// given player.
// It will return the same slice but with
// the correct player#infected set
func infectPlayer(players []player) {
  fmt.Println("Lets start!")
  fmt.Println("First we will infect a player with the virus...")

  rand.Seed(time.Now().UTC().UnixNano())
  randomNumber := rand.Intn(len(players))
  for i := range players {
    infected := i == randomNumber
    players[i].infected = infected
    if infected {
      fmt.Println("Oh no... \"" + players[i].name + "\" is infected!")
    }
  }
}

// buildMapAndPlacePlayers creates the map where the players
// are playing (and infect each other).
// It will also place the given players on this map.
// Returns the players but with player#y and player#x set.
// Also the size of the map
func buildMapAndPlacePlayers(players []player) (int , int) {
  fmt.Println("Now I'll create the map and place the players on it...")
  mapHeight := len(players) * 2
  mapWidth := len(players) * 2
  fmt.Println("The map is a square of \"" + strconv.Itoa(mapHeight) + "x" + strconv.Itoa(mapWidth) + "\".")

  for i := range players {
    rand.Seed(time.Now().UTC().UnixNano())
    players[i].y = rand.Intn(mapHeight) + 1
    players[i].x = rand.Intn(mapWidth) + 1
  }
  fmt.Println("Players are set on the map. Have fun!")
  return mapHeight, mapWidth
}

// question will ask the given question
// and returns the answer.
// If the answer is "Q" or "q" the programm
// will exit.
func question(question string) string {
  fmt.Print(question + ": ")
  answer, _ := bufio.NewReader(os.Stdin).ReadString('\n')
  answer = strings.TrimSuffix(answer, "\n")
  if answer == "Q" || answer == "q" {
    os.Exit(0)
    return ""
  }
  return answer
}
