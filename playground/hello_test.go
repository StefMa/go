package main

import(
  "testing"
)

func TestAddShouldReturnTheSum(t *testing.T) {
  want := 4
	got := add(2,2)

  if want != got {
    t.Errorf("%d is not %d", want, got)
  }
}

func TestCreatePlayerShouldReturnPlayer(t *testing.T) {
  want := player{name: "StefMa", score: 0}
	got := createPlayerWithName("StefMa")

  if want.name != got.name {
    t.Errorf("%d is not %s", want.score, got.name)
  }

  if want.score != got.score  {
    t.Errorf("%d is not %d", want.score, got.score)
  }
}

func TestUseMatherAdd(t *testing.T) {
  want := 4
	got := useMatherAdd()

  if want != got {
    t.Errorf("useMatherAdd() -> %d is not %d", want, got)
  }
}

func TestUseMatherMinus(t *testing.T) {
  want := 0
	got := useMatherMinus()

  if want != got {
    t.Errorf("useMatherMinus() -> %d is not %d", want, got)
  }
}
