set -e

if [ "$1" = "buildForMac" ]; then
  export GOOS=darwin
  export GOARCH=amd64
  go build
elif [ "$1" = "docProject" ]; then
  # Run the godoc server to create the App docs
  godoc -http=:6060&
  # Download the sources.. Found here https://git.io/fje07
  # modified by adding the /src/app
  # modified by changing the /pkg/$PKG/ to /pkg/app/
  wget -r -m -k -E -p -erobots=off --include-directories="/pkg,/lib,/src/app" --exclude-directories="*" "http://localhost:6060/pkg/app/"
  # Kill godoc afterwards
  # kill $(ps aux | grep '[g]odoc' | awk '{print $2}')
  # Rename the docs
  rm -rf docs
  mv localhost:6060 docs
  # Create redirect index.html(s)
  echo "<script>function redirect() { location.replace(\"pkg/app/index.html\")}</script> <body onload=\"redirect()\">" > docs/index.html
  # Rename localhost with the second param
  find . -type f -name '*.html' -exec sed -i "s_http://localhost:6060_$2_g" {} +
  # Remove header & footer
  find . -type f -name '*.html' -exec sed -i '/<div id="topbar"/,/<\/div><\/div>/d' {} +
  find . -type f -name '*.html' -exec sed -i '/<div id="footer"/,/<\/div>/d' {} +
else
  go $1
fi
