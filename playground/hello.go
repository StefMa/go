package main

import (
	"fmt"
	"math"
	"time"
  "app/mather"
)

var someVar = 3
const aConst = 33

func main() {
	fmt.Println(math.Pi)
  fmt.Println(add(2,2))
  fmt.Println(swap("Hello","World"))
  a, b := something()
  fmt.Print(a)
  fmt.Println(b)
  fmt.Println(split())
  anotherVar := "Hey"
  fmt.Println(someVar)
  fmt.Println(anotherVar)
  fmt.Println(aConst)
	for i := 0; i < 5; i++ {
		fmt.Print(i)
	}
	fmt.Println()
	abc := 100
	for abc < 111 {
		fmt.Println(abc)
		abc += 1
	}
	aNumber := 0
	for {
		if aNumber == 10 {
			aNumber = 20
			continue
		}
		if aNumber == 22 {
			break
		}
		fmt.Println(aNumber)
		aNumber++
	}
	if abc == 100 {
		fmt.Println("abc is 100")
	} else {
		fmt.Println("abc is not 100")
	}
	name := "Stefan"
	switch name {
	case "Lukasz":
		fmt.Println("Name is Lukasz")
	case "Stefan":
		fmt.Println("Name is Stefan")
	default:
		fmt.Println("No name :(")
	}
	fmt.Println("When's Monday?")
	today := time.Now().Weekday()
	switch {
	case today + 0 == time.Monday:
		fmt.Println("Today is Monday")
	case today + 1 == time.Monday:
		fmt.Println("Tomorrow is Monday")
	case today + 2 == time.Monday:
		fmt.Println("In two days is Monday")
	default:
		fmt.Println("Too far away.")
	}
	hello("hello")
	k,l,o := returnALot()
	fmt.Println(k)
	fmt.Println(l)
	fmt.Println(o)
	p,_,d := returnNamed()
	fmt.Println(p)
	fmt.Println(d)
	switch aNumber := 2; aNumber {
	case 2:
		fmt.Println("Number is 2")
	case 4:
		fmt.Println("Number is 4")
	}
	if a := 2222; a == 3 {
		fmt.Println("a is 3")
	} else {
		fmt.Println("a is not 3, it is", a)
	}
	// The deferred call's arguments are evaluated immediately, but the function call is not executed until the surrounding function returns.
	defer hello("StefMa")
	fmt.Println("It follows a hello(StefMa) because of the defer")
	// Deferred function calls are pushed onto a stack. When a function returns, its deferred calls are executed in last-in-first-out order.
	fmt.Println("counting")
	for i := 0; i < 10; i++ {
		defer fmt.Println(i)
	}
	fmt.Println("done")
}

func useMatherAdd() int {
  return mather.Add(2, 2)
}

func useMatherMinus() int {
  return mather.Minus(2, 2)
}

func hello(name string) {
	println("Hello " + name)
}

func returnALot() (string, int, bool) {
 return "Golicious", 42, true
}

func returnNamed() (name string, number int, boolean bool) {
 name =  "Golicious"
 number = 42
 boolean = true
 return
}

func add(x int, y int) int {
	return x + y
}

func swap(x string, y string) string {
	return y + x
}

func something() (string, int) {
	return "Something", 5
}

func split() (x, y int) {
	x = 92
	y = 77
	return
}

func createPlayerWithName(name string) player {
	return player{ name: name, score: 0 }
}

type player struct {
	name string
	score int
}
