# Go

## Docker
The [`Dockerfile`](Dockerfile) can be used to execute the `go` command.
You can put literally all commands to it when **running** the image.
This is done by the [`docker-entrypoint.sh`](docker-entrypoint.sh).

### Build the image
First you have to build the image:
```
docker build -t go .
```

### Run it
To run the image you have to put your Golang source directory
into the `/go/src/app` directory in the container.
Additionally you can add all `go` commands afterwards:
```
docker run -v ${PWD}:/go/src/app go test
```
This will put the current directory into the `/go/src/app` directory
in the container and execute `go test` there.

You can also build an **App** for mac by using:
```
docker run -v ${PWD}:/go/src/app go buildForMac
```

Creating a documentation for the current project run:
```
docker run -v ${PWD}:/go/src/app go docProject [placeholder]
```
While the `[placeholder]` should be path to your server (e.g. a url).
The output will be a directory `docs` which contains the docs for the current project...
