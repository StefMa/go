set -e

if [ "$1" = "buildForMac" ]; then
  export GOOS=darwin
  export GOARCH=amd64
  go build
else
  go $1
fi
