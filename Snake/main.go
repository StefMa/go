package main

import (
  "fmt"
  "bufio"
  "os"
  "strings"
)

const TOP = 1
const RIGHT = 2
const BOTTOM = 3
const LEFT = 4

type coordinate struct {
  x int
  y int
}

type snake struct {
  length int
  direction int
  head snakeHead
  tail []snakeTail
}

type snakeHead struct {
  position coordinate
}

type snakeTail struct {
  position coordinate
}

type game struct {
  width int
  height int
}

func main() {
  fmt.Println("Snake - a game by StefMa")

  game := game { width: 20, height: 10 }
  snakeHead := snakeHead { position: coordinate { x:3, y: 1} }
  snakeTail := []snakeTail { snakeTail { position: coordinate { x: 1, y: 1} }, snakeTail { position: coordinate { x: 2, y: 1} } }
  snake := snake { length: 1, direction: RIGHT, head: snakeHead, tail: snakeTail}
  display("", game, snake)
  for {
    text, _ := bufio.NewReader(os.Stdin).ReadString('\n')
    if strings.Contains(text, "q") {
      break
    }

    setSnakeDirection(&snake, text)
    moveSnake(&snake)
    display(text, game, snake)
  }

  fmt.Println("Bye bye... see you next time")
}

func setSnakeDirection(snake *snake, direction string) {
  switch {
  case strings.Contains(direction, "i"):
    snake.direction = TOP
  case strings.Contains(direction, "l"):
    snake.direction = RIGHT
  case strings.Contains(direction, "k"):
    snake.direction = BOTTOM
  case strings.Contains(direction, "j"):
    snake.direction = LEFT
  }
}

func moveSnake(snake *snake) {
  switch {
  case snake.direction == TOP:
    oldHeadPosition := snake.head.position
    snake.head.position.y -= 1
    snake.tail = snake.tail[:len(snake.tail) - 1]
    newTail := []snakeTail { snakeTail { oldHeadPosition } }
    newTail = append(newTail, snake.tail...)
    snake.tail = newTail
  case snake.direction == RIGHT:
    oldHeadPosition := snake.head.position
    snake.head.position.x += 1
    snake.tail = snake.tail[:len(snake.tail) - 1]
    newTail := []snakeTail { snakeTail { oldHeadPosition } }
    newTail = append(newTail, snake.tail...)
    snake.tail = newTail
  case snake.direction == BOTTOM:
    oldHeadPosition := snake.head.position
    snake.head.position.y += 1
    snake.tail = snake.tail[:len(snake.tail) - 1]
    newTail := []snakeTail { snakeTail { oldHeadPosition } }
    newTail = append(newTail, snake.tail...)
    snake.tail = newTail
  case snake.direction == LEFT:
    oldHeadPosition := snake.head.position
    snake.head.position.x -= 1
    snake.tail = snake.tail[:len(snake.tail) - 1]
    newTail := []snakeTail { snakeTail { oldHeadPosition } }
    newTail = append(newTail, snake.tail...)
    snake.tail = newTail
  }
}

func display(input string, game game, snake snake) {
  // height +2 because we want to have the height as
  // game size! Without the border...
  for i := 0; i < game.height + 2; i++ {
    isFirstOrLast := i == 0 || i == game.height + 1
    fmt.Print("|")
    for j := 0; j < game.width; j++ {
      if isFirstOrLast {
        fmt.Print("-")
      } else {
        printSnakeOrEmpty(snake, i, j)
      }
    }
    fmt.Print("|\n")
  }
}

// printSnakeOrEmpty will print the given snake.
// Y and X describes the current possition to draw...
func printSnakeOrEmpty(snake snake, y int, x int) {
  if snake.head.position.y == y && snake.head.position.x == x {
    fmt.Print("O")
    return
  }

  for i := 0; i < len(snake.tail); i++ {
    if snake.tail[i].position.y == y && snake.tail[i].position.x == x {
      fmt.Print("o")
      return
    }
  }

  fmt.Print(" ")
}
